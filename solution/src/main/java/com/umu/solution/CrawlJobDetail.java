/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umu.solution;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlHeading1;
import com.gargoylesoftware.htmlunit.html.HtmlListItem;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author umu
 */
public class CrawlJobDetail {
    public List<Departmen> departmen;
    public List<Link> links;
    
    public CrawlJobDetail(List<Departmen> departmen, List<Link> links){
        this.departmen = departmen;
        this.links = links;
    }
    
    public  List<Departmen> executeParalel()throws ExecutionException, InterruptedException{
        long t = System.currentTimeMillis();
        
        ExecutorService service= Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        
        List<JobPositionCrawl> taskList = new ArrayList<>();
        for (int i = 0; i < links.size(); i++) {
            Link link = links.get(i);
            String url = link.getLink();
            Integer departmen_id = link.getDepartmenID();
            Integer job_id = link.getJobID();
            Departmen d = departmen.get(departmen_id);
            JobPosition j = d.getJobPositions().get(job_id);
            
            JobPositionCrawl task = new JobPositionCrawl(url, j, d);
            taskList.add(task);
        }
        
        //Execute all tasks and get reference to Future objects
        List<Future<Result>> resultList = null;
 
        try {
            resultList = service.invokeAll(taskList);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
 
        service.shutdown();
        
        for (int i = 0; i < resultList.size(); i++) {
            Future<Result> future = resultList.get(i);
            try {
                Result result = future.get();
                Departmen d = departmen.get(result.departmen_id);
                d.updateJobPosition(result.jobPosition.id, result.jobPosition);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return departmen;
    
    }
    
    private class JobPositionCrawl implements Callable<Result>{
        public String link;
        public JobPosition job;
        public Departmen departmen;
    
        public JobPositionCrawl(String link, JobPosition job, Departmen departmen){
            this.link = link;
            this.job = job;
            this.departmen = departmen;
        }

        @Override
        public Result call() throws Exception {
           return crawl(link, job, departmen);
        }

}
    
    public Result crawl(String link, JobPosition job, Departmen departmen){
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        Result result = new Result();
        
        try{
            HtmlPage page = client.getPage(link);
            // job title
            HtmlHeading1 h = page.getFirstByXPath("//h1[contains(@class,'job-title')]");
            String title = h.asText();
            job.setTitle(title);
            
            // location
            HtmlElement s = page.getFirstByXPath("//span[contains(@itemprop,'address')]");
            String location = s.asText();
            job.setLocation(location);
            
            // description
            List<HtmlListItem> l_s = page.getByXPath("//section[contains(@id,'st-jobDescription')]//ul//li");
            
            ArrayList description = new ArrayList();
            for (HtmlListItem l: l_s){
                description.add(l.asText());
            }
            job.setDescription(description);
            
            // qualification
            List<HtmlListItem> l_s_2 = page.getByXPath("//section[contains(@id,'st-qualifications')]//ul//li");
            
            ArrayList qualification = new ArrayList();
            for (HtmlListItem l_2: l_s_2){
                qualification.add(l_2.asText());
            }
            job.setQualification(qualification);
            
            result.setDepartmenID(departmen.id);
            
            result.setJobPositions(job);
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
    
}

