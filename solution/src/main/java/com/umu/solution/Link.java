/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umu.solution;

/**
 *
 * @author umu
 */
public class Link {
    public Integer departmen_id;
    public Integer job_id;
    public String link;
    
    public Link(){
        this.departmen_id = null;
        this.job_id = null;
        this.link = "";
    }
    
     public void setDepartmenID(Integer id){
        this.departmen_id = id;
    }
    
    public Integer getDepartmenID(){
        return departmen_id;
    }
    
     public void setJobID(Integer id){
        this.job_id = id;
    }
    
    public Integer getJobID(){
        return job_id;
    }
    
     public void setLink(String link){
        this.link = link;
    }
    
    public String getLink(){
        return link;
    }
    
}
