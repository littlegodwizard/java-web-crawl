/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umu.solution;

/**
 *
 * @author umu
 */
public class Result {
    public JobPosition jobPosition;
    public Integer departmen_id;
    
     public Result(){
        this.departmen_id = null;
        this.jobPosition = null;
    }
    
    public void setDepartmenID(Integer id){
        this.departmen_id = id;
    }
    
    public Integer getDepartmenID(){
        return departmen_id;
    }
    
    public void setJobPositions(JobPosition position){
        this.jobPosition = position;
    }
    
    public JobPosition getJobPosition(){
        return jobPosition;
    }
    
}
