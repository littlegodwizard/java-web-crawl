/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umu.solution;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomAttr;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlListItem;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author umu
 */
public class Main {
    public static List<Departmen> departmen = new ArrayList();
    public static List<Link> links = new ArrayList();
    public static final String url = "https://www.cermati.com/karir";

    /**
     * @param args the command line arguments
     * @return 
     */
    public static void main(String[] args) {
        System.out.println("Pease wait.........");
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        
        try{
            HtmlPage page = client.getPage(url);
        
            List<HtmlListItem> departmens = page.getByXPath("//div[contains(@id,'section-vacancies')]//div[contains(@class,'career-tab')]//ul//li");
            
            for(int i = 0; i < departmens.size(); i++){
                HtmlElement p = departmens.get(i).getElementsByTagName("p").get(0);
                String departmen_name = p.asText();
                
                Departmen team = new Departmen();
                team.setDepartmenName(departmen_name);
                team.setID(i);
                
                departmen.add(team);
            }
            
            for(int i = 0; i < departmen.size(); i++) {
                HtmlDivision tab = page.getHtmlElementById("tab"+i);
                List<DomAttr> container = tab.getByXPath(String.format("//div[contains(@id,'section-vacancies')]//div[contains(@class,'career-tab')]//div[contains(@class, 'tab-content')]//div[contains(@id, 'tab%d')]//div[contains(@class,'container-fluid')]//a//@href", i));
                           
                Departmen d = departmen.get(i);
                
                Integer idx = 0;
                for(DomAttr a: container){
                    String link = a.getValue();
                    
                    JobPosition job_position = new JobPosition();
                    job_position.setID(idx);
                    d.addJobPosition(job_position);
                    
                    Link l = new Link();
                    l.setLink(link);
                    l.setJobID(idx);
                    l.setDepartmenID(d.getID());
                    links.add(l);
                    
                    idx++;
                }                   
            }
            
            departmen = new CrawlJobDetail(departmen, links).executeParalel();
            JsonArray result = resultToJson(departmen);
            
            HashMap<String, Boolean> config = new HashMap<String, Boolean>();
            config.put(JsonGenerator.PRETTY_PRINTING, true);

            JsonWriterFactory jwf = Json.createWriterFactory(config);
            StringWriter sw = new StringWriter();

            try (JsonWriter jsonWriter = jwf.createWriter(sw)) {
                jsonWriter.writeArray(result);
                try (FileWriter file = new FileWriter("solution.json")) {
 
                file.write(sw.toString());
                file.flush();
                System.out.println("Success: result generated in solution.json");
 
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
              

        }
        catch(Exception e){
            e.printStackTrace();
        }
        
}
    private static JsonArray resultToJson(List<Departmen> departmens){
        JsonArrayBuilder result = Json.createArrayBuilder();
        
        JsonObjectBuilder ob = Json.createObjectBuilder();
        departmens.forEach(departmen -> {
            JsonArrayBuilder jobs = Json.createArrayBuilder();
            departmen.jobPositions.forEach(job -> {
                JsonObjectBuilder j = Json.createObjectBuilder();
                j.add("title", job.title);
                j.add("location", job.location);
                
                JsonArrayBuilder d = Json.createArrayBuilder();
                job.description.forEach(desc -> {
                    d.add(desc);
                });
                j.add("description", d);
                
                JsonArrayBuilder q = Json.createArrayBuilder();
                job.qualification.forEach(qa -> {
                    q.add(qa);
                });
                j.add("qualification", q);
                j.add("posted by", job.posted_by);
                jobs.add(j);
            });
            ob.add(departmen.departmenName, jobs);
            
        });
        result.add(ob);
        
        return result.build();

    }

}

