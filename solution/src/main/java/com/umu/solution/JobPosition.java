/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umu.solution;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author umu
 */
public class JobPosition {
    public Integer id;
    public String title;
    public String location;
    public List<String> description;
    public List<String> qualification;
    public String posted_by;
    
    public JobPosition(){
        this.id = null;
        this.title = "";
        this.location = "";
        this.description = new ArrayList();
        this.qualification = new ArrayList();
        this.posted_by = "";        
    }
    
    public void setID(Integer id){
        this.id = id;
    }
    
    public Integer getID(){
        return id;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public String getTitle(){
        return title;
    }
    
    public void setLocation(String location){
        this.location= location;
    }
    
    public String getLocation(){
        return location;
    }
    
    public void setDescription(List<String> description){
        this.description = description;
    }
    
    public List<String> getDescription(){
        return description;
    }
    
    public void setQualification(List<String> qualification){
        this.qualification = qualification;
    }
    
    public List<String> getQualification(){
        return qualification;
    }
    
    public void setPoster(String poster){
        this.posted_by = poster;
    }
    
    public String getPoster(){
        return posted_by;
    }
    
    public void addDescription(String description){
        this.description.add(description);
    }
    
    public void addQualification(String qualification){
        this.qualification.add(qualification);
    }
    
}
