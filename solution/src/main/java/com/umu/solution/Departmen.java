/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umu.solution;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author umu
 */
public class Departmen {
    public Integer id;
    public String departmenName;
    public List<JobPosition> jobPositions;
    
    public Departmen(){
        this.id = null;
        this.departmenName = "";
        this.jobPositions = new ArrayList();
    }
    
    public void setID(Integer id){
        this.id = id;
    }
    
    public Integer getID(){
        return id;
    }
    
    public void setDepartmenName(String departmen_name){
        this.departmenName = departmen_name;
    }
    
    public String getDepartmenName(){
        return departmenName;
    }
    
    public void setJobPositions(List<JobPosition> positions){
        this.jobPositions = positions;
    }
    
    public List<JobPosition> getJobPositions(){
        return jobPositions;
    }
    
    public void addJobPosition(JobPosition postion){
        this.jobPositions.add(postion);
    }
    
    public void updateJobPosition(Integer idx, JobPosition position){
        this.jobPositions.set(idx, position);
    }
    
}
